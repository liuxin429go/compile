#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os
import sys

import SCons

from .fs import pushd

LOGGER = logging.getLogger("H2D2.compile.patch.scons")

QUOTE_SPACES_PATCHED = [
    'def quote_spaces(arg):',
    '    """Generic function for putting double quotes around any string that',
    '    has white space in it.',
    '',
    '    YSe - Fix',
    '    The patched version uses shlex.split to detect only unquoted spaces',
    '    """',
    '    import shlex',
    '    arg = str(arg)',
    '    if (\' \' in arg or \'\\t\' in arg) and len(shlex.split(arg)) > 1:',
    '        return \'"%s"\' % arg',
    '    else:',
    '        return arg',
    '',
    ]

def get_scons_version():
    return SCons.__version__
    
def get_scons_dir():
    return os.path.dirname(SCons.__file__)

def patch_quote_spaces():
    """
    """
    LOGGER.warning("SCons.Subst.quote_spaces must be patched")

    # ---  Modify file Subst.py
    sconsroot = get_scons_dir()
    with pushd(sconsroot):
        fic = "Subst.py"
        ficOri = '.'.join( (fic, 'ori', get_scons_version()) )
        ficNew = '.'.join( (fic, 'new') )
        if not os.path.isfile(ficOri):
            lines = []
            with open(fic, "r") as ifs:
                LOGGER.trace("Modify %s in %s" % (fic, os.getcwd()))
                for line in ifs:
                    line = line.rstrip()
                    ls = line.strip()
                    if ls == "def quote_spaces(arg):":
                        for l in QUOTE_SPACES_PATCHED:
                            lines.append(l)
                        lines.append("def quote_spaces_YSe_replaced(arg):")
                    else:
                        lines.append(line)

            with open(ficNew, "w") as f:
                f.write("\n".join(lines))
                f.write("\n")

            if os.path.isfile(ficNew):
                os.rename(fic, ficOri)
                os.rename(ficNew, fic)
        else:
            LOGGER.info("SCons already patched")

def patch_vs2017():
    """
    scons 3.0.3 doesn't detect correctly VS 2017. This should be corrected
    in version 3.0.4
    """
    try:
        from SCons.Script import EnsureSConsVersion
        EnsureSConsVersion(3, 0, 4)
        return
    except SystemExit as e:
        LOGGER.warning("SCons.Tool.MSCommon.vc must be patched for Visual Studio 2017")
        
    # ---  Modify file vc.py
    sconsroot = get_scons_dir()
    with pushd(os.path.join(sconsroot, "Tool", "MSCommon")):
        from SCons import __version__ as SCons_version
        fic = "vc.py"
        ficOri = '.'.join( (fic, 'ori', SCons_version) )
        ficNew = '.'.join( (fic, 'new') )
        if not os.path.isfile(ficOri):
            lines = []
            with open(fic, "r") as ifs:
                LOGGER.trace("Modify %s in %s" % (fic, os.getcwd()))
                for line in ifs:
                    line = line.rstrip()
                    ls = line.strip()
                    if ls == "installed_versions.append(ver)":
                        lines.append(line)
                        lines.append(4*"    " + "elif find_vc_pdir_vswhere(ver): # YSe - Added for VS 2017")
                        lines.append(4*"    " + "    installed_versions.append(ver)")
                    else:
                        lines.append(line)

            with open(ficNew, "w") as f:
                f.write("\n".join(lines))
                f.write("\n")

            if os.path.isfile(ficNew):
                os.rename(fic, ficOri)
                os.rename(ficNew, fic)
        else:
            LOGGER.info("SCons already patched")


def patchSCons():
    patch_vs2017()
    patch_quote_spaces()
    
if __name__ == "__main__":
    import SCons.Subst
    a = 'abc should be quoted'
    print(a, '\there=%s' % quote_spaces(a), '\tscons=%s' % SCons.Subst.quote_spaces(a))
    a = '"abc should not be quoted"'
    print(a, '\there=%s' % quote_spaces(a), '\tscons=%s' % SCons.Subst.quote_spaces(a))
    
    patch_quote_spaces()
    
    a = '"abc should not be quoted"'
    print(a, '\there=%s' % quote_spaces(a), '\tscons=%s' % SCons.Subst.quote_spaces(a))

