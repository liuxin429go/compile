#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import collections.abc
import logging
import subprocess
import six

LOGGER = logging.getLogger("H2D2.compile.call")

# ---  Syntactic sugar: Remove the need for user to import subprocess
CalledProcessError = subprocess.CalledProcessError

#==============================================================================
#==============================================================================
def isIterable(arg):
    """
    Return True if arg is iterable but not a string
    https://stackoverflow.com/questions/1055360/how-to-tell-a-variable-is-iterable-but-not-a-string
    """
    return isinstance(arg, collections.abc.Iterable) and not isinstance(arg, six.string_types)

#==============================================================================
#==============================================================================
def doCallOrRaise(command, **kwargs):
    """
    Return stdout of the command, or raise
    """
    def decode(s, encodings=('ascii', 'utf8', 'latin1')):
        """
        https://stackoverflow.com/questions/269060/is-there-a-python-library-function-which-attempts-to-guess-the-character-encodin
        """
        try:
            return s.decode(decode.lastEncoding)
        except:
            for encoding in encodings:
                try:
                    r = s.decode(encoding)
                    decode.lastEncoding = encoding
                    return r
                except UnicodeDecodeError:
                    pass
        return s.decode('ascii', 'ignore')

    doLog = kwargs.pop('doLog', True)
    if isIterable(command):
        cmd = ' '.join(command)
    else:
        cmd = command
    o = []
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, **kwargs)
    if doLog:
        LOGGER.dump("Output for command: %s", cmd)
    while p.poll() is None:
        l = decode(p.stdout.readline().rstrip())
        if l:
            o.append(l)
            if doLog: LOGGER.dump('   %s' % l)
    for l in p.stdout.readlines():
        l = decode(l.rstrip())
        if l:
            o.append(l)
            if doLog: LOGGER.dump('   %s' % l)
    if p.returncode != 0:
        raise CalledProcessError(p.returncode, cmd)

    return '\n'.join(o)

    
if __name__ == "__main__":
    from context     import getTestContext
    from addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)
    
    doCallOrRaise('pwd')
            