# H2D2 compilation scripts

This is the entry point to an compilation/installation of H2D2

Using the scripts, a typical Linux install sequence would be:

```bash
    create a directory in user space
    cd to this directory

    git clone --recursive --branch branche_1904 -v https://gitlab.com/h2d2/compile.git
    cd compile
    python 00-00-all.py --help
    python 00-00-all.py --build H2D2 ...
    python 00-00-all.py --build H2D2-tools [...]
    cd ..
    cd install
    python 00-00-all.py --help
    python 00-00-all.py --build H2D2 [...]
    python 00-00-all.py --build H2D2-tools [...]
```

For a default install in `~/opt`, you may want to modify your `PATH`. You also need
to check that environment variable `TMP` point to a valid temporary directory.
Typicaly:

```bash
export PATH=~/opt/miniconda3/bin:~/opt/H2D2/bin:~/opt/H2D2-tools:$PATH
export TMP=/tmp
```
