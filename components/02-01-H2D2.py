#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os

import tools.fs  as tl_fs
import tools.git as tl_git

LOGGER = logging.getLogger("H2D2.compile.download.H2D2")

def gitCloneAll(ctx, cwd='.'):
    tl_git.gitLabCloneBranch("build",   ctx.branch, cwd=cwd)
    tl_git.gitLabCloneBranch("install", ctx.branch, cwd=cwd)
    if ctx.isH2D2():
        tl_git.gitLabCloneBranch("toolbox",    ctx.branch, cwd=cwd)
        #tl_git.gitLabCloneBranch("tutorials",  ctx.branch, cwd=cwd)
        tl_git.gitLabCloneBranch("H2D2",       ctx.branch, cwd=cwd)
    elif ctx.isH2D2Tools():
        tl_git.gitLabCloneBranch("H2D2-tools", ctx.branch, cwd=cwd)
    elif ctx.isH2D2Remesh():
        tl_git.gitLabCloneBranch("H2D2-Remesh",ctx.branch, cwd=cwd)
    elif ctx.isH2D2OpenDA():
        tl_git.gitLabCloneBranch("H2D2-OpenDA",ctx.branch, cwd=cwd)

def xeq(ctx):
    # ---  Header
    LOGGER.info("H2D2 - git clone H2D2")
    LOGGER.debug("   pwd: %s ", os.getcwd())

    # ---  Skip if nogit
    if ctx.nogit:
        LOGGER.debug("   skipped")
        return

    # ---  Pre-conditions
    assert ctx.target,  "ctx.target must be defined"
    assert ctx.branch,  "ctx.branch must be defined"
    assert ctx.baseDir, "ctx.baseDir must be defined"

    # ---  Git clone
    try:
        gitCloneAll(ctx, cwd=ctx.baseDir)
    except:
        exit()

    # ---  Footer
    LOGGER.debug("   pwd: %s ", os.getcwd())
    LOGGER.info("H2D2 - git clone H2D2: Done")


if __name__ == "__main__":
    from tools.context     import getTestContext
    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    xeq(ctx)

