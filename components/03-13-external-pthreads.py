#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import shutil
import os

import tools.call  as tl_call
import tools.fs    as tl_fs
import tools.ptf   as tl_ptf
import tools.url   as tl_url

LOGGER = logging.getLogger("H2D2.compile.download.external.pthreads4w")

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Download files
    pkgDir = "pthreads4w-code-07053a521b0a9deb6db2a649cde1f828f2eb1f4f"
    pkgFic = "pthreads4w-code-v3.0.0.zip"
    pkgUrl = "https://sourceforge.net/projects/pthreads4w/files"
    if not os.path.isfile(pkgFic):
        try:
            # requests download the SF intermediate page
            # download with curl works directly
            # https://sourceforge.net/p/forge/documentation/Downloading%20files%20via%20the%20command%20line/
            tl_call.doCallOrRaise(["curl -L", "-o %s" % pkgFic, "%s/%s/%s" % (pkgUrl, pkgFic, "download")])
        except tl_call.CalledProcessError as e:
            if os.path.isfile(pkgFic): os.remove(pkgFic)
            raise RuntimeError("Failed downloading %s" % pkgFic)

    # ---  unzip to directory
    if not os.path.isdir(pkgDir):
        assert os.path.isfile(pkgFic)
        try:
            tl_fs.unzipFile(pkgFic)
        except Exception as e:
            LOGGER.warning("deleting invalid file %s" % pkgFic)
            if os.path.isfile(pkgFic): os.remove(pkgFic)
            if os.path.isdir (pkgDir): shutil.rmtree(pkgDir)
            raise

    if os.path.isdir(pkgDir):
        with tl_fs.pushd(pkgDir):
            cpl_one = 'cpl_one.bat'
            if not os.path.isfile(cpl_one):
                cmds=[
                    'nmake realclean',
                    'nmake all VC',
                    'nmake DESTROOT="..\\pthreads4w-v3.0.0" install',
                ]
                with open(cpl_one, 'wt') as f:
                    f.write("\n".join(cmds))
                    f.write("\n")

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))
    lxtLib = "pthreads4w"

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return
    if not tl_ptf.isWindows(): return

    # ---  Header
    LOGGER.info("Download external lib: %s" % lxtLib)

    # ---  Pre-conditions
    assert os.environ["INRS_LXT"], "INRS_LXT must be defined"

    # ---  Do the job
    with tl_fs.pushd(os.environ["INRS_LXT"]):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("Download external lib: %s: Done", lxtLib)

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    os.environ['INRS_BLD'] = os.path.join(ctx.baseDir, "h2d2.build")
    os.environ['INRS_LXT'] = os.path.join(ctx.baseDir, "external")
    os.environ['INRS_DEV'] = os.environ['INRS_LXT']
    print(ctx)
    xeq(ctx)
