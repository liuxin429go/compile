#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import distutils
import logging
import shutil
import os

import tools.fs      as tl_fs
import tools.git     as tl_git
import tools.url     as tl_url

LOGGER = logging.getLogger("H2D2.compile.download.external.ARPACK")

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Download files
    pkgDir = "ARPACK"
    pkgFics= ["arpack96.tar.gz", "patch.tar.gz"]
    pkgUrl = "https://www.caam.rice.edu/software/ARPACK/SRC"
    for pkgFic in pkgFics:
        if not os.path.isfile(pkgFic):
            isOk = tl_url.urlDownload(pkgUrl + "/" + pkgFic)
            if not isOk:
                if os.path.isfile(pkgFic): os.remove(pkgFic)
                raise RuntimeError("Failed downloading %s" % pkgFic)

    # ---  untar to directory
    if not os.path.isdir(pkgDir):
        try:
            for pkgFic in pkgFics:
                tl_fs.untarFile(pkgFic)
        except (tl_fs.TarError, EOFError) as e:
            for pkgFic in pkgFics:
                LOGGER.warning("deleting invalid file %s" % pkgFic)
                if os.path.isfile(pkgFic): os.remove(pkgFic)
            if os.path.isdir (pkgDir): shutil.rmtree(pkgDir)
            raise

    # ---  Modify file second.f
    with tl_fs.pushd(os.path.join(pkgDir, "UTIL")):
        LOGGER.trace("second.f in %s" % os.getcwd())
        fic = "second.f"
        ficOri = fic+".ori"
        ficNew = fic+".new"
        if not os.path.isfile(ficOri):
            lines = []
            with open(fic, "r") as ifs:
                LOGGER.trace("Modify %s in %s" % (fic, os.getcwd()))
                for line in ifs:
                    line = line.rstrip()
                    ls = line.strip()
                    if ls.split() == ["EXTERNAL", "ETIME"]:
                        lines.append("!! Out-commented for H2D2 compilation")
                        lines.append("!!%s" % line)
                    else:
                        lines.append(line)

            with open(ficNew, "w") as f:
                f.write("\n".join(lines))
                f.write("\n")

            if os.path.isfile(ficNew):
                os.rename(fic, ficOri)
                os.rename(ficNew, fic)

    # ---  git clone
    gitGrp = "external"
    gitLib = "ARPACK-build"
    gitDir = "/".join([gitGrp, gitLib])
    tl_git.gitLabCloneMaster(gitDir, cwd=".")

    # ---  Copy to compile directory
    tl_fs.copytree(gitLib, pkgDir, ignore=shutil.ignore_patterns(".*"))

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return

    # ---  Header
    LOGGER.info("Download external lib: ARPACK")

    # ---  Pre-conditions
    assert os.environ["INRS_LXT"], "INRS_LXT must be defined"

    # ---  Do the job
    with tl_fs.pushd(os.environ["INRS_LXT"]):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("Download external lib: ARPACK: Done")


if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    os.environ['INRS_LXT'] = os.path.join(ctx.baseDir, "external")
    print(ctx)
    xeq(ctx)
