#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import glob
import logging
import shutil
import os

import tools.fs      as tl_fs
import tools.call    as tl_call
import tools.git     as tl_git
import tools.url     as tl_url

LOGGER = logging.getLogger("H2D2.compile.download.external.scalapack")

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Download files
    #pkgDir = "scalapack-2.0.2"
    pkgDir  = "scalapack-2.1.0"
    pkgInst = "scalapack_installer"
    pkgDirs = [pkgDir, pkgInst]
    pkgFics = ["%s.tgz" % pkgDir, "%s.tgz" % pkgInst]
    pkgUrl = "http://www.netlib.org/scalapack"
    for pkgFic in pkgFics:
        if not os.path.isfile(pkgFic):
            isOk = tl_url.urlDownload(pkgUrl + "/" + pkgFic)
            if not isOk:
                if os.path.isfile(pkgFic): os.remove(pkgFic)
                raise RuntimeError("Failed downloading %s" % pkgFic)

    # ---  untar to directory
    for pDir, pFic in zip(pkgDirs, pkgFics):
        if os.path.isdir(pDir): continue
        try:
            tl_fs.untarFile(pFic)
        except (tl_fs.TarError, EOFError) as e:
            LOGGER.warning("deleting invalid file %s" % pFic)
            if os.path.isfile(pFic): os.remove(pFic)
            if os.path.isdir (pDir): shutil.rmtree(pDir)
            raise

    # ---  Python 2 to 3
    with tl_fs.pushd("scalapack_installer"):
        for src in glob.iglob('**/*.py', recursive=True):
            if not os.path.isfile(src + ".bak"):
                tl_call.doCallOrRaise(["2to3", "--no-diffs", "--write", src ])

    # ---  git clone
    gitGrp = "external"
    gitLib = "scalapack-build"
    gitDir = "/".join([gitGrp, gitLib])
    tl_git.gitLabCloneMaster(gitDir)

    # ---  Copy to compile directory
    LOGGER.trace("scalapack: rsync")
    tl_fs.copytree(gitLib, "scalapack_installer", ignore=shutil.ignore_patterns(".*"))

    # ---  Modify file setup.py
    with tl_fs.pushd("scalapack_installer"):
        fic = "setup.py"
        ficOri = fic+".ori"
        ficNew = fic+".new"
        if not os.path.isfile(ficOri):
            lines = []
            with open(fic, "r") as ifs:
                LOGGER.trace("Modify %s in %s" % (fic, os.getcwd()))
                for line in ifs:
                    line = line.rstrip()
                    line = line.replace('\t', '    ')
                    lines.append(line)

            with open(ficNew, "w") as f:
                f.write("\n".join(lines))
                f.write("\n")

            if os.path.isfile(ficNew):
                os.rename(fic, ficOri)
                os.rename(ficNew, fic)

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))
    lxtLib = "scalapack"

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return

    # ---  Header
    LOGGER.info("Download external lib: %s" % lxtLib)

    # ---  Pre-conditions
    assert os.environ["INRS_LXT"], "INRS_LXT must be defined"

    # ---  Do the job
    with tl_fs.pushd(os.environ["INRS_LXT"]):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("Download external lib: %s: Done", lxtLib)


if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    os.environ['INRS_LXT'] = os.path.join(ctx.baseDir, "external")
    xeq(ctx)
