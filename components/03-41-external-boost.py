#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import shutil
import os

import tools.fs      as tl_fs
import tools.git     as tl_git
import tools.url     as tl_url

LOGGER = logging.getLogger("H2D2.compile.download.external.boost")

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Download files
    pkgDir = "boost_1_73_0"
    pkgFic = "%s.tar.gz" % pkgDir.lower()
    pkgUrl = "https://dl.bintray.com/boostorg/release/1.73.0/source"
    if not os.path.isfile(pkgFic):
        isOk = tl_url.urlDownload(pkgUrl+"/"+ pkgFic)
        if not isOk:
            if os.path.isfile(pkgFic): os.remove(pkgFic)
            raise RuntimeError("Failed downloading %s" % pkgFic)

    # ---  untar to directory
    if not os.path.isdir(pkgDir):
        assert os.path.isfile(pkgFic)
        try:
            tl_fs.untarFile(pkgFic)
        except (tl_fs.TarError, EOFError) as e:
            LOGGER.warning("deleting invalid file %s" % pkgFic)
            if os.path.isfile(pkgFic): os.remove(pkgFic)
            if os.path.isdir (pkgDir): shutil.rmtree(pkgDir)
            raise

    # ---  git clone
    gitGrp = "external"
    gitLib = "boost-build"
    gitDir = "/".join([gitGrp, gitLib])
    tl_git.gitLabCloneMaster(gitDir, cwd=".")

    # ---  Copy to compile directory
    tl_fs.copytree(gitLib, pkgDir, ignore=shutil.ignore_patterns(".*"))

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))
    lxtLib = "boost"

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2Remesh(): return
    # check for platform

    # ---  Header
    LOGGER.info("Download external lib: %s" % lxtLib)

    # ---  Pre-conditions
    assert os.environ["INRS_LXT"], "INRS_LXT must be defined"

    # ---  Do the job
    with tl_fs.pushd(os.environ["INRS_LXT"]):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("Download external lib: %s: Done", lxtLib)

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    os.environ['INRS_LXT'] = os.path.join(ctx.baseDir, "external")
    print(ctx)
    xeq(ctx)
