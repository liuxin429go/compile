#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import glob
import logging
import os

import tools.ptf     as tl_ptf
import tools.fs      as tl_fs
import tools.call    as tl_call

LOGGER = logging.getLogger("H2D2.compile.build.external.pthreads4w")

def doWork(ctx, lxtLib):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Compile
    cpl_one = "cpl_one.bat"
    for pkgDir in glob.glob("%s-code-*" % lxtLib):
        pkgTgt = "pthreads4w-v3.0.0"
        if os.path.isdir(pkgDir):
            if not os.path.isdir(pkgTgt):
                LOGGER.info("Building "+ pkgDir)
                tl_call.doCallOrRaise([cpl_one, ctx.complr, ctx.MPIlib, ctx.integer], cwd=pkgDir)
            else:
                LOGGER.info("%s is detected as up to date.", pkgTgt)
                LOGGER.info("To force recompilation, delete directory %s", pkgTgt)


def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))
    lxtLib = "pthreads4w"

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return
    if not tl_ptf.isWindows(): return

    # ---  Header
    LOGGER.info("Build external lib: %s" % lxtLib)

    # ---  Pre-conditions
    assert os.environ["INRS_LXT"], "INRS_LXT must be defined"
    assert os.environ["INRS_BLD"], "INRS_BLD must be defined"

    # ---  Compile
    with tl_fs.pushd(os.environ["INRS_LXT"]):
        doWork(ctx, lxtLib)

    # ---  Footer
    LOGGER.info("Build external lib: %s: Done", lxtLib)

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    xeq(ctx)
