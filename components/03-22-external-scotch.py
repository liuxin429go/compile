#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import shutil
import os

import tools.fs      as tl_fs
import tools.git     as tl_git
import tools.url     as tl_url

LOGGER = logging.getLogger("H2D2.compile.download.external.scotch")

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Download files
    pkgDir = "scotch_6.0.4"
    pkgUrl = "https://gforge.inria.fr/frs/download.php/file/34618"
    # pkgDir = "scotch_6.0.6"
    # pkgUrl = "https://gforge.inria.fr/frs/download.php/file/37622"
    # pkgDir = "scotch_6.0.7"
    # pkgUrl = "https://gforge.inria.fr/frs/download.php/file/38040"
    # pkgDir = "scotch_6.0.8"
    # pkgUrl = "https://gforge.inria.fr/frs/download.php/file/38114"
    # pkgDir = "scotch_6.0.9"
    # pkgUrl = "https://gforge.inria.fr/frs/download.php/file/38187"
    # pkgDir = "scotch_6.1.0"
    # pkgUrl = "https://gforge.inria.fr/frs/download.php/file/38352"
    pkgFic = "%s.tar.gz" % pkgDir
    if not os.path.isfile(pkgFic):
        isOk = tl_url.urlDownload(pkgUrl+"/"+ pkgFic)
        if not isOk:
            if os.path.isfile(pkgFic): os.remove(pkgFic)
            raise RuntimeError("Failed downloading %s" % pkgFic)

    # ---  untar to directory
    if not os.path.isdir(pkgDir):
        assert os.path.isfile(pkgFic)
        try:
            tl_fs.untarFile(pkgFic)
        except (tl_fs.TarError, EOFError) as e:
            LOGGER.warning("deleting invalid file %s" % pkgFic)
            if os.path.isfile(pkgFic): os.remove(pkgFic)
            if os.path.isdir (pkgDir): shutil.rmtree(pkgDir)
            raise

    # ---  Modify file common.h
    with tl_fs.pushd(os.path.join(pkgDir, "src", "libscotch")):
        fic = "common.h"
        ficOri = fic+".ori"
        ficNew = fic+".new"
        if not os.path.isfile(ficOri):
            lines = []
            with open(fic, "r") as ifs:
                LOGGER.trace("Modify %s in %s" % (fic, os.getcwd()))
                for line in ifs:
                    line = line.rstrip()
                    ls = line.strip()
                    if ls.split()[0:2] == ["ThreadBarrier", "barrdat;"]:
                        lines.append(line)
                        lines.append("#else")
                        lines.append("  /*")
                        lines.append("    YSe: Added for Windows without pthread")
                        lines.append("    thrdnbr is use in graph_coarsen.c, line 352")
                        lines.append("  */")
                        lines.append("  int                       thrdnbr;              /*+ Number of threads        +*/")
                    else:
                        lines.append(line)

            with open(ficNew, "w") as f:
                f.write("\n".join(lines))
                f.write("\n")

            if os.path.isfile(ficNew):
                os.rename(fic, ficOri)
                os.rename(ficNew, fic)

    # ---  git clone
    gitGrp = "external"
    gitLib = "scotch-build"
    gitDir = "/".join([gitGrp, gitLib])
    tl_git.gitLabCloneMaster(gitDir, cwd=".")

    # ---  Copy to compile directory
    tl_fs.copytree(gitLib, pkgDir, ignore=shutil.ignore_patterns(".*"))

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))
    lxtLib = "scotch"

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return

    # ---  Header
    LOGGER.info("Download external lib: %s" % lxtLib)

    # ---  Pre-conditions
    assert os.environ["INRS_LXT"], "INRS_LXT must be defined"

    # ---  Do the job
    with tl_fs.pushd(os.environ["INRS_LXT"]):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("Download external lib: %s: Done", lxtLib)

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.compile")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    os.environ['INRS_BLD'] = os.path.join(ctx.baseDir, "h2d2.build")
    os.environ['INRS_LXT'] = os.path.join(ctx.baseDir, "external")
    os.environ['INRS_DEV'] = os.environ['INRS_LXT']
    print(ctx)
    xeq(ctx)
